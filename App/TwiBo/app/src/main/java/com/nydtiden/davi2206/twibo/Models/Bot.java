package com.nydtiden.davi2206.twibo.Models;

import java.sql.Time;

/**
 * Created by David on 15-Jul-17.
 */

public class Bot
{
    public int ID;
    public String name;
    public String ruleSubstring;
    public String ruleHashtag;
    public String ruleUser;
    public String ruleArea;
    public boolean ruleRetweets;
    public boolean ruleFollowback;
    public int ruleRetweetPercent;
    public int ruleRetweetFollowersPercent;
    public boolean ruleFollowersOnly;
    public String signature;
    public int ruleRTtime;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuleSubstring() {
        return ruleSubstring;
    }

    public void setRuleSubstring(String ruleSubstring) {
        this.ruleSubstring = ruleSubstring;
    }

    public String getRuleHashtag() {
        return ruleHashtag;
    }

    public void setRuleHashtag(String ruleHashtag) {
        this.ruleHashtag = ruleHashtag;
    }

    public String getRuleUser() {
        return ruleUser;
    }

    public void setRuleUser(String ruleUser) {
        this.ruleUser = ruleUser;
    }

    public String getRuleArea() {
        return ruleArea;
    }

    public void setRuleArea(String ruleArea) {
        this.ruleArea = ruleArea;
    }

    public boolean isRuleRetweets() {
        return ruleRetweets;
    }

    public void setRuleRetweets(boolean ruleRetweets) {
        this.ruleRetweets = ruleRetweets;
    }

    public boolean isRuleFollowback() {
        return ruleFollowback;
    }

    public void setRuleFollowback(boolean ruleFollowback) {
        this.ruleFollowback = ruleFollowback;
    }

    public int getRuleRetweetPercent() {
        return ruleRetweetPercent;
    }

    public void setRuleRetweetPercent(int ruleRetweetPercent) {
        this.ruleRetweetPercent = ruleRetweetPercent;
    }

    public int getRuleRetweetFollowersPercent() {
        return ruleRetweetFollowersPercent;
    }

    public void setRuleRetweetFollowersPercent(int ruleRetweetFollowersPercent) {
        this.ruleRetweetFollowersPercent = ruleRetweetFollowersPercent;
    }

    public boolean isRuleFollowersOnly() {
        return ruleFollowersOnly;
    }

    public void setRuleFollowersOnly(boolean ruleFollowersOnly) {
        this.ruleFollowersOnly = ruleFollowersOnly;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getRuleRTtime() {
        return ruleRTtime;
    }

    public void setRuleRTtime(int ruleRTtime) {
        this.ruleRTtime = ruleRTtime;
    }

    public Bot(int id, String name, String ruleSubstring, String ruleHashtag, String ruleUser, String ruleArea, boolean ruleRetweets, boolean ruleFollowback, int ruleRetweetPercent, int ruleRetweetFollowersPercent, boolean ruleFollowersOnly, String signature, int ruleRTtime) {
        this.ID = id;
        this.name = name;
        this.ruleSubstring = ruleSubstring;
        this.ruleHashtag = ruleHashtag;
        this.ruleUser = ruleUser;
        this.ruleArea = ruleArea;
        this.ruleRetweets = ruleRetweets;
        this.ruleFollowback = ruleFollowback;
        this.ruleRetweetPercent = ruleRetweetPercent;
        this.ruleRetweetFollowersPercent = ruleRetweetFollowersPercent;
        this.ruleFollowersOnly = ruleFollowersOnly;
        this.signature = signature;
        this.ruleRTtime = ruleRTtime;
    }

    public Bot(){}
}
