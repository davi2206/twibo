package com.nydtiden.davi2206.twibo.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.nydtiden.davi2206.twibo.Models.Bot;
import com.nydtiden.davi2206.twibo.Models.User;

import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by David on 16-Jul-17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "twibo_data.db";
    SQLiteDatabase db;

    public static final String TABLE_USERS = "users";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_RTCOUNT = "rtcount";
    public static final String COLUMN_DATA = "data";
    public static final String COLUMN_NOTIFY = "notifyonerror";

    public static final String TABLE_BOTS = "bots";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_RULESUBSTRING = "rulesubstring";
    public static final String COLUMN_RULEHASHTAG = "rulehashtag";
    public static final String COLUMN_RULEFROMUSER = "rulefromuser";
    public static final String COLUMN_RULEFROMAREA = "rulefromarea";
    public static final String COLUMN_RULERTRTS = "rulertrts";
    public static final String COLUMN_RULEFOLLOWBACK = "rulefollowbac";
    public static final String COLUMN_RULEFOLLOWERSONLY = "rulefollowersonly";
    public static final String COLUMN_RULERTCHANCE = "rulertchance";
    public static final String COLUMN_RULEPERCENTCHANCEFOLLOWERS = "rulepercentchancefollowers";
    public static final String COLUMN_SIGNATURE = "signature";
    public static final String COLUMN_SCHEDULE = "schedule";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String usersTable = String.format("CREATE TABLE " +
                        "%s (" +
                        "%s TEXT PRIMARY KEY," +
                        "%d INT,)," +
                        "%d INT," +
                        "%d INT" +
                        ");",
                TABLE_USERS,
                COLUMN_USERNAME,
                COLUMN_RTCOUNT,
                COLUMN_DATA,
                COLUMN_NOTIFY);

        String botsTable = String.format("CREATE TABLE" +
                        "%s (" +
                        "%d INT PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT, " +
                        "%s TEXT, " +
                        "%s TEXT, " +
                        "%s TEXT, " +
                        "%s TEXT, " +
                        "%d INT, " +
                        "%d INT, " +
                        "%d INT, " +
                        "%d INT, " +
                        "%d INT, " +
                        "%s TEXT, " +
                        "%d INT, " +
                        ");",
                TABLE_BOTS,
                COLUMN_ID,
                COLUMN_NAME,
                COLUMN_RULESUBSTRING,
                COLUMN_RULEHASHTAG,
                COLUMN_RULEFROMUSER,
                COLUMN_RULEFROMAREA,
                COLUMN_RULERTRTS,
                COLUMN_RULEFOLLOWBACK,
                COLUMN_RULEFOLLOWERSONLY,
                COLUMN_RULERTCHANCE,
                COLUMN_RULEPERCENTCHANCEFOLLOWERS,
                COLUMN_SIGNATURE,
                COLUMN_SCHEDULE);

        sqLiteDatabase.execSQL(usersTable);
        sqLiteDatabase.execSQL(botsTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String dropUsers = String.format("DROP TABLE IF EXISTS %s", TABLE_USERS);
        String dropBots = String.format("DROP TABLE IF EXISTS %s", TABLE_BOTS);;

        sqLiteDatabase.execSQL(dropUsers);
        sqLiteDatabase.execSQL(dropBots);

        onCreate(sqLiteDatabase);
    }

    public void addUser(User user){
        db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, user.getName());
        values.put(COLUMN_RTCOUNT, user.getRtCount());
        values.put(COLUMN_DATA, user.isUseData());
        values.put(COLUMN_NOTIFY, user.isNotifyOnError());

        db.insert(TABLE_USERS, null, values);
        db.close();
    }

    public void updateUser(User user){
        db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, user.getName());
        values.put(COLUMN_RTCOUNT, user.getRtCount());
        values.put(COLUMN_DATA, user.isUseData());
        values.put(COLUMN_NOTIFY, user.isNotifyOnError());

        String where = String.format("%s = %s", COLUMN_USERNAME, user.getName());

        db.update(TABLE_USERS, values, where, null);
        db.close();
    }

    public void addBot(Bot bot){
        db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, bot.getName());
        values.put(COLUMN_RULESUBSTRING, bot.getRuleSubstring());
        values.put(COLUMN_RULEHASHTAG, bot.getRuleHashtag());
        values.put(COLUMN_USERNAME, bot.getRuleUser());
        values.put(COLUMN_RULEFROMAREA, bot.getRuleArea());
        values.put(COLUMN_RULERTRTS, bot.isRuleRetweets());
        values.put(COLUMN_RULEFOLLOWBACK, bot.isRuleFollowback());
        values.put(COLUMN_RULEFOLLOWERSONLY, bot.isRuleFollowersOnly());
        values.put(COLUMN_RULERTCHANCE, bot.getRuleRetweetPercent());
        values.put(COLUMN_RULEPERCENTCHANCEFOLLOWERS, bot.getRuleRetweetFollowersPercent());
        values.put(COLUMN_SIGNATURE, bot.getSignature());
        values.put(COLUMN_SCHEDULE, bot.getRuleRTtime());

        db.insert(TABLE_BOTS, null, values);
        db.close();
    }

    public void updateBot(Bot bot){
        db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, bot.getName());
        values.put(COLUMN_RULESUBSTRING, bot.getRuleSubstring());
        values.put(COLUMN_RULEHASHTAG, bot.getRuleHashtag());
        values.put(COLUMN_USERNAME, bot.getRuleUser());
        values.put(COLUMN_RULEFROMAREA, bot.getRuleArea());
        values.put(COLUMN_RULERTRTS, bot.isRuleRetweets());
        values.put(COLUMN_RULEFOLLOWBACK, bot.isRuleFollowback());
        values.put(COLUMN_RULEFOLLOWERSONLY, bot.isRuleFollowersOnly());
        values.put(COLUMN_RULERTCHANCE, bot.getRuleRetweetPercent());
        values.put(COLUMN_RULEPERCENTCHANCEFOLLOWERS, bot.getRuleRetweetFollowersPercent());
        values.put(COLUMN_SIGNATURE, bot.getSignature());
        values.put(COLUMN_SCHEDULE, bot.getRuleRTtime());

        String where = String.format("%s = %s", COLUMN_ID, bot.getID());

        db.update(TABLE_BOTS, values, where, null);
        db.close();
    }

    public User getUser(){
        User user = null;
        db = getWritableDatabase();

        String getUser = String.format("SELECT * FROM %s", TABLE_USERS);

        Cursor c = db.rawQuery(getUser, null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            String username = c.getString(c.getColumnIndex(COLUMN_USERNAME));
            if(username != null){
                int rtCount = c.getInt(c.getColumnIndex(COLUMN_RTCOUNT));
                boolean useData = (c.getInt(c.getColumnIndex(COLUMN_DATA)) == 1);
                boolean notifyError = c.getInt(c.getColumnIndex(COLUMN_NOTIFY)) == 1;
                user = new User(username, rtCount, useData, notifyError);
            }
        }

        db.close();
        return user;
    }

    public Bot getBot(int id){
        Bot bot = null;
        db = getWritableDatabase();

        String getBot = String.format("SELECT * FROM %s WHERE %s = %d", TABLE_BOTS, COLUMN_ID, id);

        Cursor c = db.rawQuery(getBot, null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            int db_id = c.getInt(c.getColumnIndex(COLUMN_ID));
            String name = c.getString(c.getColumnIndex(COLUMN_NAME));
            String subString = c.getString(c.getColumnIndex(COLUMN_RULESUBSTRING));
            String hashtag = c.getString(c.getColumnIndex(COLUMN_RULEHASHTAG));
            String user = c.getString(c.getColumnIndex(COLUMN_USERNAME));
            String area = c.getString(c.getColumnIndex(COLUMN_RULEFROMAREA));
            boolean rts = (c.getInt(c.getColumnIndex(COLUMN_RULERTRTS)) == 1);
            boolean followback = c.getInt(c.getColumnIndex(COLUMN_RULEFOLLOWBACK)) == 1;
            int rtPercent = c.getInt(c.getColumnIndex(COLUMN_RULERTCHANCE));
            int rtFollowerPercent = c.getInt(c.getColumnIndex(COLUMN_RULEPERCENTCHANCEFOLLOWERS));
            boolean followersOnly = c.getInt(c.getColumnIndex(COLUMN_RULEFOLLOWERSONLY)) == 1;
            String signature = c.getString(c.getColumnIndex(COLUMN_SIGNATURE));
            int rtTime = c.getInt(c.getColumnIndex(COLUMN_SCHEDULE));

            bot = new Bot(db_id, name, subString, hashtag,
                    user, area, rts,
                    followback, rtPercent, rtFollowerPercent,
                    followersOnly, signature, rtTime);
        }

        db.close();

        return bot;
    }

    public Map getBots(){
        Map bots = new HashMap();
        db = getWritableDatabase();

        String getBots = String.format("SELECT * FROM %s", TABLE_BOTS);

        Cursor c = db.rawQuery(getBots, null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            int db_id = c.getInt(c.getColumnIndex(COLUMN_ID));
            String name = c.getString(c.getColumnIndex(COLUMN_NAME));

            bots.put(db_id, name);
        }

        db.close();

        return bots;
    }

    public void deleteBot(Bot bot){
        db = getWritableDatabase();
        String delete = String.format("DELETE FROM %s WHERE %s = %d", TABLE_BOTS, COLUMN_ID, bot.getID());

        db.execSQL(delete);
        db.close();
    }
}



