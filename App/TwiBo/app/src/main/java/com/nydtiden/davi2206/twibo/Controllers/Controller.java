package com.nydtiden.davi2206.twibo.Controllers;

import com.nydtiden.davi2206.twibo.Models.*;
import com.nydtiden.davi2206.twibo.R;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

import java.util.GregorianCalendar;
import java.util.Map;

/**
 * Created by David on 16-Jul-17.
 */

public class Controller {
    private Context context;
    private DatabaseHandler dbh;

    public Controller(Context context){
        this.context = context;
        dbh = new DatabaseHandler(context, null, null, 1, null);
    }

    public void addUser(User user){
        dbh.addUser(user);
    }

    public User getUser(){
        User user = dbh.getUser();
        return user;
    }

    public void updateUser(User user){
        dbh.updateUser(user);
    }

    public void addBot(Bot bot){
        Long hours = (long) bot.getRuleRTtime();
        int botId = bot.getID();
        setBotAlarm(hours, botId);
        dbh.addBot(bot);
    }

    public Bot getBot(int id){
        Bot bot = dbh.getBot(id);
        return bot;
    }

    public Map getBots(){
        Map bots = dbh.getBots();
        return bots;
    }

    public void updateBot(Bot bot){
        dbh.updateBot(bot);
    }

    public void deleteBot(Bot bot){

        dbh.deleteBot(bot);
    }

    //TODO
    public void setBotAlarm(Long hours, int botId){
        Long ms = hours*60*60*1000;
        Long waitTime = new GregorianCalendar().getTimeInMillis()+ms;

        Intent alertIntent = new Intent(context, *AlertManager ectends BroadcastReceiver*);
        alertIntent.putExtra("BotId", botId);

        AlarmManager alarmMan = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        alarmMan.set(AlarmManager.RTC_WAKEUP, waitTime,
                PendingIntent.getBroadcast(
                        context, 1, alertIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT));

        //Get alarm manager
        //Start countdown
    }

    //TODO
    public void runBot(int bot){
        //Get tweets from rules
        //Get tweet IDs
        //RT tweets
        //Restart alarm
        //If error check for notify
        // If notify -> notify


    }
}
