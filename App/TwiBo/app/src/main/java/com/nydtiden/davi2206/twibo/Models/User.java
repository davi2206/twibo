package com.nydtiden.davi2206.twibo.Models;

/**
 * Created by David on 15-Jul-17.
 */

public class User {
    public String name;
    public int rtCount;
    public boolean useData;
    public boolean notifyOnError;

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRtCount() {
        return rtCount;
    }

    public void setRtCount(int rtCount) {
        this.rtCount = rtCount;
    }

    public boolean isUseData() {
        return useData;
    }

    public void setUseData(boolean useData) {
        this.useData = useData;
    }

    public boolean isNotifyOnError() {
        return notifyOnError;
    }

    public void setNotifyOnError(boolean notifyOnError) {
        this.notifyOnError = notifyOnError;
    }

    public User(String name, int rtCount, boolean useData, boolean notifyOnError) {
        this.name = name;
        this.rtCount = rtCount;
        this.useData = useData;
        this.notifyOnError = notifyOnError;
    }

    public User(){}
}
