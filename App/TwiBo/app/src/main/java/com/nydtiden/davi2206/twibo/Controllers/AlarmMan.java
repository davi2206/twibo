package com.nydtiden.davi2206.twibo.Controllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

import com.nydtiden.davi2206.twibo.Models.Bot;

/**
 * Created by David on 17-Jul-17.
 */

public class AlarmMan extends BroadcastReceiver {

    Controller ctrl;
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        int botId = intent.getIntExtra("BotId", 0);

        try{
            ctrl.runBot(botId);
        }
        catch (Exception e){
            Bot bot = ctrl.getBot(botId);
            notifyError(bot);
        }
    }

    private void notifyError(Bot bot){
        String errorMessage = String.format("An error occoured when trying to run the bot: %s", bot.getName());
        String errorTitle = "TwiBo encountered an error";

        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(errorTitle)
                .setContentText(errorMessage)
                .setTicker("TwiBo Error")
                .setSmallIcon();
    }
}
