# README #

Short description of the project. Explaining the project and the plans for it. 

### What is this repository for? ###
*Project summary*
* This project is for educational purpuses. It is made for me to explore some of the features in Android

*Application Summary*
* The application is a manager for twitter bots. They can be made to retweet based on substrings, hashtags, areas, users and more. 
* In the application you set up bots to retweet based on your rules

*Version*
* 0.0

### How do I get set up? ###
The application will be available on Google Play, when it is ready for release

### Contribution guidelines ###
Don't